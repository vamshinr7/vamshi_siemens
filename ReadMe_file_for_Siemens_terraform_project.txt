Read me file
********************************
META DATA
# Author: Vamshikrishna for Siemens
# date: 28/02/2024
# version: v1.0.0
# description: Creating Loadbalancer with EC2 autoscaling
and VPC with both public attched to internetgateway(IGW) and 
private subnet attched to NAT gateway with respective route tables
associated. EC2 is in private subnet, and 
Httpd hosted in ec2 can be accesed with laodbalancer generated DNS.

#Email : vamshinr7gmail.com && vamshinr6@gmail.com

region: ap-south-1 mumbai
             

***********************
Pre-requisites

	1.install aws cli
	2.install terraform
      3.install git and VS code(any terminal)   
      4.connect to AWS console via aws-config
      5.git clone the the project vamshi_siemens.zip and cd vamshi_siemens 
      6.generate Access key and secrete key and 

#######################
      7.update main.tf, provider block update your Access_key and secrete_key

        provider "aws" {
  region                   = "ap-south-1"
  access_key = "AKIxxxxxxxxxxxxx"
  secret_key = "X7Cxxxxxxxxxxxxxxxxxxxxxx"

*************************
      Commands to execute in VSCODE(any terminal)

      8. goto the folder where you have .tf filesin you laptop and performed terraform commands below
	   "terraform init"
	   "terraform plan"
	   "terraform apply"      -yes
      
      9. after some time login to aws console and check the following are created

      Note***: DO NOT CHANGE any STATE of ANY resource in AWS console, because it may cause 
          "terraform.tfstate" file mismatch which eventually couses problems while destroying the resources

          a) go to loadbalancer: "vamshi-lb-asg" copy DNS :"vamshi-lb-asgxxxxxxxxxxxxx"
             paste it in any of your favarite browser  you will see httpd(apache)
             you can see "Hello World, Siemens this is VAMSHI KRISHNA xxxxxxxxxx xxxxx"
 
      10. if you need you can check following services are created in background
          VPC with public subnet and private subnet
          IGW, NAT GW, Route tables, load balncer with target group, EC2 with Autoscale group(ASG)
           
          
      11. once you are done with confirmation you use command 
          " terraform destroy"   -yes
           


      12. it will delete all the resources created by terraform

Disclaimer: Due to time constraint I could not complete "AnsibleProvisioner" integration with terraform.
            but, I can proudly say i have included and met rest of the requiremnt provided by Siemens

 Thanks Hema for the giving me an opportunity brushup my knowledge

*******************************************************************
  